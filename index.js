const express = require('express')
const {HostList} = require('./utils/HostList')
const CronJob = require('cron').CronJob
const server = express()
const hosts = new HostList()

server.use((request, response, next) => {
  response.setHeader('Access-Control-Allow-Origin', '*')
  response.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS, POST')
  response.setHeader(
      'Access-Control-Allow-Headers',
      'X-Requested-With,content-type',
  )
  response.setHeader('Access-Control-Allow-Credentials', 'true')
  next()
})

const taskUpdateHosts = new CronJob('0 */5 * * * *', () => hosts.update())
taskUpdateHosts.start()

server.get('/get-hosts/', function(request, res) {
  res.json(hosts.getHosts)
})

server.listen(8091)
