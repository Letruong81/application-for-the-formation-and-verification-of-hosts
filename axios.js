const https = require('https')
const axios = require('axios').default
const httpsAgent = new https.Agent({
  rejectUnauthorized: false,
})

const instance = axios.create({
  httpsAgent: httpsAgent,
  timeout: 1000,
})

module.exports = instance