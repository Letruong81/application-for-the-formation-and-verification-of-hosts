## Utilities FOIL

| Name | URL | Description |
| ------ | ------ | ------ |
| Hosts | [Utils:get-hosts](https://utils.foil.network/get-hosts) | Formation of a list of hosts with indication of block height and availability. Sort the list by response time. |